#include "include/bmp.h"
#include "include/bmpio.h"
#include "include/imagetransform.h"
#include "include/interop.h"
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char **argv) {
  (void)argc;
  (void)argv; // supress 'unused parameters' warning

  if (argc < 3) {
    printf("%d args given, but 2 was expected, see usage: image-transformer "
           "<input.bmp> <output.bmp>\n",
           argc - 1);
    return EXIT_FAILURE;
  }

  char *input_filepath = argv[1];
  char *output_filepath = argv[2];

  bmp_image_ptr original_bmp;
  enum read_status rs = bmp_image_read_from_file(input_filepath, &original_bmp);
  if (rs != READ_OK) {
    if (rs == READ_FILE_UNAVAILABLE) {
      fprintf(stderr, "File %s is unavailable", input_filepath);
    } else if (rs == READ_UNEXPECTED_EOF) {
      fprintf(stderr, "Unexpected EOF %s found", input_filepath);
    } else if (rs == READ_FILE_ERROR) {
      fprintf(stderr, "Some error while reading %s occurred", input_filepath);
    } else if (rs == READ_MEMORY_ALLOCATION_ERROR) {
      fprintf(stderr, "Memory allocations failed");
    } else {
      fprintf(stderr, "Unexpected read error occurred");
    }
    return EXIT_FAILURE;
  }

  // TODO: Error handling, what if bmp_image is invalid?
  struct image *original_image = bmp_image_to_image(original_bmp);
  bmp_image_free(original_bmp);

  struct image *rotated_image =
      image_rotate_90_degrees_counterclockwise(original_image);
  image_free(original_image);

  bmp_image_ptr rotated_bmp = image_to_bmp_image(rotated_image);
  image_free(rotated_image);

  enum write_status ws = bmp_image_write_to_file(output_filepath, rotated_bmp);
  if (ws != WRITE_OK) {
    if (ws == WRITE_FILE_ERROR) {
      fprintf(stderr, "Error while writting to %s occurred", input_filepath);
    } else if (ws == WRITE_FILE_UNAVAILABLE) {
      fprintf(stderr, "File %s is unavailable", input_filepath);
    } else {
      fprintf(stderr, "Unexpected write error occurred");
    }
  }

  bmp_image_free(rotated_bmp);

  return EXIT_SUCCESS;
}
