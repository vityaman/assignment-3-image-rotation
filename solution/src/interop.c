#include "include/interop.h"
#include "include/bmp.h"
#include "include/image.h"
#include "include/util.h"
#include <stdbool.h>
#include <string.h>

const uint16_t BITS_PER_PIXEL = 8 * sizeof(struct pixel);
const uint32_t BI_RGB = 0;
const uint32_t PIXELS_PER_METER = 2834;

size_t image_data_row_padding(size_t image_width) { return image_width % 4; }

size_t image_data_row_size(struct image const *img) {
  return sizeof(struct pixel) * img->width + image_data_row_padding(img->width);
}

uint32_t image_data_size(struct image const *img) {
  return image_data_row_size(img) * img->height;
}

size_t image_file_size(struct image const *img) {
  return sizeof(struct bmp_file_headers) + sizeof(struct bmp_info_headers) +
         image_data_size(img);
}

struct image *bmp_image_to_image(bmp_image_ptr bmp) {
  struct bmp_info_headers *bmp_info = &bmp_image_headers(bmp)->info;
  struct image *image = image_new(bmp_info->width, bmp_info->height);
  uint8_t *bmp_data = bmp_image_data(bmp);
  const uint32_t data_row_size = image_data_row_size(image);
  for (uint32_t row = 0; row < image->height; ++row) {
    memcopy_reversed(image_row(image, row), bmp_data + row * data_row_size,
            sizeof(struct pixel) * image->width);
  }
  return image;
}

void image_fill_bmp_info_headers(struct image const *img,
                                 struct bmp_info_headers *info_headers) {
  info_headers->size = sizeof(struct bmp_info_headers);
  info_headers->width = img->width;
  info_headers->height = img->height;
  info_headers->color_planes_count = 1;
  info_headers->bits_per_pixel = BITS_PER_PIXEL;
  info_headers->compression = BI_RGB;
  info_headers->image_size = image_data_size(img);
  info_headers->x_pixels_count_per_meter = PIXELS_PER_METER;
  info_headers->y_pixels_count_per_meter = PIXELS_PER_METER;
  info_headers->used_colors_count = 0;
  info_headers->important_colors_count = 0;
}

void image_fill_bmp_file_headers(struct image const *img,
                                 struct bmp_file_headers *file_headers) {
  file_headers->file_type[0] = 'B';
  file_headers->file_type[1] = 'M';
  file_headers->reserved_0 = 0;
  file_headers->reserved_1 = 0;
  file_headers->file_size = image_file_size(img);
  file_headers->data_offset =
      sizeof(struct bmp_file_headers) + sizeof(struct bmp_info_headers);
}

void image_fill_bmp_data(struct image const *img, uint8_t *data) {
  const size_t data_row_size = image_data_row_size(img);
  for (size_t row = 0; row < img->height; ++row) {
    memcopy_reversed(data + row * data_row_size, image_row(img, row),
            sizeof(struct pixel) * img->width);
  }
}

bmp_image_ptr image_to_bmp_image(struct image const *img) {
  uint8_t *raw_image = calloc(image_file_size(img), 1);

  struct bmp_info_headers *info_headers =
      (struct bmp_info_headers *)(raw_image + sizeof(struct bmp_file_headers));
  image_fill_bmp_info_headers(img, info_headers);

  struct bmp_file_headers *file_headers =
      (struct bmp_file_headers *)(raw_image + 0);
  image_fill_bmp_file_headers(img, file_headers);

  uint8_t *image_data = (raw_image + file_headers->data_offset);
  image_fill_bmp_data(img, image_data);

  return (bmp_image_ptr){raw_image};
}
