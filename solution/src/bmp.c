#include "include/bmp.h"
#include <stdlib.h>

struct bmp_headers *bmp_image_headers(bmp_image_ptr bmp) {
  return (struct bmp_headers *)bmp.raw;
}

uint8_t *bmp_image_data(bmp_image_ptr bmp) {
  const uint32_t data_offset = bmp_image_headers(bmp)->file.data_offset;
  return bmp.raw + data_offset;
}

void bmp_image_free(bmp_image_ptr bmp) { free(bmp.raw); }
