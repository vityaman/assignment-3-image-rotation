#ifndef _BMP_IO_H_
#define _BMP_IO_H_

#include "bmp.h"
#include <stdio.h>

enum read_status {
  READ_OK = 0,
  READ_UNEXPECTED_EOF,
  READ_FILE_ERROR,
  READ_FILE_UNAVAILABLE,
  READ_MEMORY_ALLOCATION_ERROR,
};

enum write_status { WRITE_OK = 0, WRITE_FILE_ERROR, WRITE_FILE_UNAVAILABLE };

enum read_status bmp_image_read_from(FILE *in, bmp_image_ptr *bmp);
enum write_status bmp_image_write_to(FILE *out, bmp_image_ptr bmp);
enum read_status bmp_image_read_from_file(char *filepath, bmp_image_ptr *bmp);
enum write_status bmp_image_write_to_file(char *filepath, bmp_image_ptr bmp);

#endif
