#ifndef _IMAGETRANSFORM_H_
#define _IMAGETRANSFORM_H_

#include "image.h"

struct image *
image_rotate_90_degrees_counterclockwise(struct image const *origin);

#endif
