#ifndef _INTEROP_H_
#define _INTEROP_H_

#include "bmp.h"
#include "image.h"
#include <stdbool.h>

struct image *bmp_image_to_image(bmp_image_ptr bmp);
bmp_image_ptr image_to_bmp_image(struct image const *img);

#endif
