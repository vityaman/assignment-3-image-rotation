#include "include/bmpio.h"
#include "include/bmp.h"
#include "include/util.h"
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

enum read_status
bmp_image_file_headers_read_from(FILE *in, struct bmp_file_headers *headers) {
  size_t count = fread(headers, sizeof(struct bmp_file_headers), 1, in);
  if (count < 1) {
    if (feof(in)) {
      return READ_UNEXPECTED_EOF;
    }
    return READ_FILE_ERROR;
  }
  return READ_OK;
}

enum read_status bmp_image_data_read_from(FILE *in, uint8_t *dest,
                                          uint32_t data_size) {
  size_t count = fread(dest, data_size, 1, in);
  if (count < 1) {
    if (feof(in)) {
      return READ_UNEXPECTED_EOF;
    }
    return READ_FILE_ERROR;
  }
  return READ_OK;
}

enum read_status bmp_image_read_from(FILE *in, bmp_image_ptr *bmp) {
  const uint32_t bmp_file_headers_size = sizeof(struct bmp_file_headers);

  struct bmp_file_headers file_headers;
  enum read_status status = bmp_image_file_headers_read_from(in, &file_headers);
  if (status != READ_OK) {
    return status;
  }

  uint8_t *raw_image = malloc(file_headers.file_size);
  if (!raw_image) {
    return READ_MEMORY_ALLOCATION_ERROR;
  }

  memcopy(raw_image, &file_headers, bmp_file_headers_size);

  uint8_t *data_ptr = raw_image + bmp_file_headers_size;
  const uint32_t data_size = file_headers.file_size - bmp_file_headers_size;
  status = bmp_image_data_read_from(in, data_ptr, data_size);
  if (status != READ_OK) {
    return status;
  }

  bmp->raw = raw_image;

  return READ_OK;
}

enum write_status bmp_image_write_to(FILE *out, bmp_image_ptr bmp) {
  uint32_t file_size = bmp_image_headers(bmp)->file.file_size;
  size_t count = fwrite(bmp.raw, file_size, 1, out);
  if (count < 1) {
    return WRITE_FILE_ERROR;
  }
  return WRITE_OK;
}

enum read_status bmp_image_read_from_file(char *filepath, bmp_image_ptr *bmp) {
  FILE *in = fopen(filepath, "rb");
  if (!in) {
    return READ_FILE_UNAVAILABLE;
  }
  bool read_status = bmp_image_read_from(in, bmp);
  fclose(in);
  return read_status;
}

enum write_status bmp_image_write_to_file(char *filepath, bmp_image_ptr bmp) {
  FILE *out = fopen(filepath, "wb");
  if (!out) {
    return WRITE_FILE_UNAVAILABLE;
  }
  bool write_status = bmp_image_write_to(out, bmp);
  fclose(out);
  return write_status;
}
