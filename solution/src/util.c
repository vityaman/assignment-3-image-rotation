#include "include/util.h"
#include "inttypes.h"

static size_t index_identity(size_t i, size_t n) {
  return i;
}

static size_t index_reversed(size_t i, size_t n) {
  return n - 1 - i;
}

// TODO: use memcpy_s instead of it
void memcopy(void *dest, void *src, size_t n) {
  _memcopy(dest, src, n, &index_identity);
}

void memcopy_reversed(void *dest, void *src, size_t n) {
  _memcopy(dest, src, n, &index_reversed);
}

static void _memcopy(
  void *dest, void *src, size_t n, 
  size_t (*index_mapper)(size_t i, size_t n)
) {
  uint8_t *csrc = (uint8_t *)src;
  uint8_t *cdest = (uint8_t *)dest;
  for (size_t i = 0; i < n; i++) {
    cdest[i] = csrc[index_mapper(i, n)];
  }
}
